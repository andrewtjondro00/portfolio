from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from .models import Entry
from .forms import EntryForm

def index(request):
    if request.method == "POST":
        form = EntryForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['message']

            Entry.objects.create(
                name=name,
            ).save()

            return HttpResponseRedirect('/')
    else:
        form = EntryForm()

    n = Entry.objects.all();

    return render(request, 'templates/myproject/form.html', {'form': form, 'n': n})
